/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.shape;

/**
 *
 * @author f
 */
public class TestShape {
    public static void main(String[] args) {
        Circle c1 = new Circle(1.5);
        Circle c2 = new Circle(4.5);
        Circle c3 = new Circle(5.5);
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);
        Rectangle r1 = new Rectangle(3,4);
        Rectangle r2 = new Rectangle(4,8);
        Rectangle r3 = new Rectangle(2,7);
        System.out.println(r1);
        System.out.println(r2);
        System.out.println(r3);
        Square s1 = new Square(5);
        Square s2 = new Square(6.5);
        Square s3 = new Square(8.9);
        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s3);
        
        Shape[] shapes = {c1, c2, c3, r1, r2, r3, s1, s2, s3};
        for(int i=0; i<shapes.length; i++){
            System.out.println(shapes[i].getName() +" area: "+ shapes[i].calArea());
        }
    }
}
